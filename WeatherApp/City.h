//
//  City.h
//  WeatherApp
//
//  Created by Алексей on 21.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (strong, nonatomic) NSString *cityName;
@property (strong, nonatomic) NSString *cityCode;
@property (strong, nonatomic) NSString *cityDescription;

-(City *)initWithDict:(NSDictionary *)dict;

@end
