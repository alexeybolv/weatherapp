//
//  Weather+CoreDataProperties.m
//  WeatherApp
//
//  Created by Алексей on 23.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Weather+CoreDataProperties.h"

@implementation Weather (CoreDataProperties)

@dynamic weatherCityName;
@dynamic weatherDescription;
@dynamic weatherMain;
@dynamic weatherTemp;
@dynamic weatherLastUpdate;

@end
