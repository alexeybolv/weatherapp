//
//  Weather.h
//  WeatherApp
//
//  Created by Алексей on 20.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Weather : NSManagedObject

+ (Weather *)createWeatherWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context;
- (void)changeWeatherWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Weather+CoreDataProperties.h"
