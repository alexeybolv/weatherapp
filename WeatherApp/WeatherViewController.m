//
//  WeatherViewController.m
//  WeatherApp
//
//  Created by Алексей on 21.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "WeatherViewController.h"
#import "WeatherCityTableViewCell.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NetworkManager.h"
#import "Weather.h"
#import "City.h"

@interface WeatherViewController () <UITableViewDelegate,UITableViewDataSource>



@property (weak, nonatomic) IBOutlet UIView *cityView;
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;

//Outlet Info
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherTemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherMainLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeightConstraint;


@property (strong,nonatomic) NSMutableArray <City*> *citiesArray;
@property (strong,nonatomic) Weather *currentWeather;
@property (strong,nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation WeatherViewController

- (NSMutableArray *)citiesArray {
    if (!_citiesArray) _citiesArray = [NSMutableArray array];
    return _citiesArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addGesture];
    [self clearAllOutlets];
    [self parseFile];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.citiesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherCityTableViewCell *cityCell = [tableView dequeueReusableCellWithIdentifier:@"WeatherCityCellIdentifier" forIndexPath:indexPath];
    [cityCell setCellWithCity:[self.citiesArray objectAtIndex:indexPath.row]];
    return cityCell;
}

#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    self.currentWeather = nil;
    City *selectedCity = [self.citiesArray objectAtIndex:indexPath.row];
    self.cityNameLabel.text = selectedCity.cityName;
    self.cityCodeLabel.text = selectedCity.cityCode;
    self.cityDescriptionLabel.text = selectedCity.cityDescription;
    [self getWeatherInfoWithCityName:selectedCity.cityName];
}

#pragma mark - GetWeatherInfo

-(void) getWeatherInfoWithCityName:(NSString *)cityName
{
    NSPredicate *predicateForWeather = [NSPredicate predicateWithFormat:@"weatherCityName == %@", cityName];
    self.currentWeather = [Weather MR_findFirstWithPredicate:predicateForWeather];
    double lastUpdateTime = [self.currentWeather.weatherLastUpdate timeIntervalSince1970];
    double currentTime = [[NSDate date] timeIntervalSince1970];
    
    if ((currentTime-lastUpdateTime > 3600)||(self.currentWeather == nil))
    {
        if([NetworkManager sharedNetworkManager].lastTask)
        {
            [[NetworkManager sharedNetworkManager].lastTask cancel];
        }
        [[NetworkManager sharedNetworkManager] getWeatherWithCityName:cityName andCompletion:^(NSDictionary *data, NSString *error)
         {
             if(data)
             {
                 [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext)
                  {
                      self.currentWeather = [Weather MR_findFirstWithPredicate:predicateForWeather inContext:localContext];
                      if (self.currentWeather)
                      {
                          [self.currentWeather changeWeatherWithDictionary:data inContext:localContext];
                      }
                      else
                      {
                          self.currentWeather = [Weather createWeatherWithDictionary:data inContext:localContext];
                      }
                  } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                      self.currentWeather = [Weather MR_findFirstWithPredicate:predicateForWeather];
                      [self setWeatherOutletsWithWeather:self.currentWeather];
                  }];
             }
             else
             {
                 self.currentWeather = [Weather MR_findFirstWithPredicate:predicateForWeather];
                 if(self.currentWeather)
                 {
                     [self setWeatherOutletsWithWeather:self.currentWeather];
                 }
                 else
                 {
                     [self hideWeatherInfoWithArrow:NO];
                 }
             }
         }];
    }
    else
    {
        [self setWeatherOutletsWithWeather:self.currentWeather];
    }
}


#pragma mark - ParseLocalFile

-(void) parseFile
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"document" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSError *error =  nil;
    NSDictionary *jsonDataDictionary = [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    if(!error)
    {
        NSArray *cities = [jsonDataDictionary objectForKey:@"Cities"];
        for(id item in cities)
        {
            City *newCity = [[City alloc] initWithDict:item];
            [self.citiesArray addObject:newCity];
        }
    }
}

#pragma mark - ConfigureGestureRecognizer

- (void)addGesture
{
    UITapGestureRecognizer * objTapGesture = [self createTapGestureOnView:self.cityView];
    [objTapGesture addTarget:self action:@selector(imageTapped:)];
}

- (NSLayoutConstraint *)changeMultiplier:(NSLayoutConstraint *)constraint to:(CGFloat)newMultiplier{
    NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:(constraint).firstItem attribute:(constraint).firstAttribute relatedBy:(constraint).relation toItem:(constraint).secondItem attribute:(constraint).secondAttribute multiplier:newMultiplier constant:(constraint).constant];
    [self.view removeConstraint:(constraint)];
    [self.view addConstraint:newConstraint];
    return newConstraint;
}

- (void)imageTapped:(UIGestureRecognizer*)recognizer
{
    NSString *multiplierString = [NSString stringWithFormat:@"%.2f",(double)self.tableviewHeightConstraint.multiplier];
    if([multiplierString doubleValue] == 0.4)
    {
        [self hideWeatherInfoWithArrow:YES];
    }
    else if((self.selectedIndexPath)&&(self.currentWeather))
    {
        [self showWeatherInfo];
    }

}

- (UITapGestureRecognizer*)createTapGestureOnView:(UIView *)view
{
    view.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]init];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [view addGestureRecognizer:tapGesture];
    return tapGesture;
}

#pragma mark - ClearOutlets

- (void)clearWeatherOutlets
{
    self.weatherTemperatureLabel.text = @"";
    self.weatherMainLabel.text = @"";
    self.weatherDescriptionLabel.text = @"";
}

- (void)clearAllOutlets
{
    self.cityNameLabel.text = @"";
    self.cityCodeLabel.text = @"";
    self.cityDescriptionLabel.text = @"";
    self.arrowImageView.image = nil;
    self.weatherTemperatureLabel.text = @"";
    self.weatherMainLabel.text = @"";
    self.weatherDescriptionLabel.text = @"";
}

#pragma mark - SetWeatherOutlets

- (void)setWeatherOutletsWithWeather:(Weather *)currentWeather
{
    NSString *multiplierString = [NSString stringWithFormat:@"%.2f",(double)self.tableviewHeightConstraint.multiplier];
    if([multiplierString doubleValue] == 0.4)
    {
        self.arrowImageView.image = [UIImage imageNamed:@"arrow_up.png"];
        self.weatherTemperatureLabel.text = [NSString stringWithFormat:@"%i °C",[currentWeather.weatherTemp intValue]];
        self.weatherMainLabel.text = currentWeather.weatherMain;
        self.weatherDescriptionLabel.text = currentWeather.weatherDescription;
    }
    else
    {
        self.arrowImageView.image = [UIImage imageNamed:@"arrow_down.png"];
    }
}

#pragma mark - HideWeatherInfo

- (void)hideWeatherInfoWithArrow:(BOOL) showArrow
{
    if(showArrow)
    {
        self.arrowImageView.image = [UIImage imageNamed:@"arrow_down.png"];
    }
    else
    {
        self.arrowImageView.image = nil;
    }
    self.tableviewHeightConstraint = [self changeMultiplier:self.tableviewHeightConstraint to:0.67];
    [self clearWeatherOutlets];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - ShowWeatherInfo

- (void)showWeatherInfo
{
    self.arrowImageView.image = [UIImage imageNamed:@"arrow_up.png"];
    self.tableviewHeightConstraint = [self changeMultiplier:self.tableviewHeightConstraint to:0.4];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    [self setWeatherOutletsWithWeather:self.currentWeather];
}

@end
