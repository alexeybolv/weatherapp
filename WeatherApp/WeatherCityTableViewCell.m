//
//  WeatherCityTableViewCell.m
//  WeatherApp
//
//  Created by Алексей on 21.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "WeatherCityTableViewCell.h"

@implementation WeatherCityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellWithCity:(City *)city
{
    self.cityNameLabel.text = city.cityName;
    self.cityCodeLabel.text = city.cityCode;
}


@end
