//
//  NetworkManager.h
//  WeatherApp
//
//  Created by Алексей on 20.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface NetworkManager : AFHTTPSessionManager

@property (strong,nonatomic) NSURLSessionDataTask *lastTask;

+ (NetworkManager *)sharedNetworkManager;
-(void) getWeatherWithCityName:(NSString *)cityName andCompletion:(void(^)(NSDictionary *data, NSString *error))completion;

@end
