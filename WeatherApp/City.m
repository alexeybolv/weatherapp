//
//  City.m
//  WeatherApp
//
//  Created by Алексей on 21.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "City.h"

@implementation City

-(City *)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        self.cityName = [dict objectForKey:@"Name"];
        self.cityCode = [dict objectForKey:@"Code"];
        self.cityDescription = [dict objectForKey:@"Description"];
    }
    return self;
}


@end
