//
//  NetworkManager.m
//  WeatherApp
//
//  Created by Алексей on 20.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "NetworkManager.h"

static NSString *baseURL = @"http://api.openweathermap.org";
static NSString *AppID = @"96f6c15f795066aaabedaea09b627a6f";

@implementation NetworkManager

+ (NetworkManager *)sharedNetworkManager {
    static NetworkManager *sharedNetworkManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNetworkManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    });
    return sharedNetworkManager;
}

-(void) getWeatherWithCityName:(NSString *)cityName andCompletion:(void(^)(NSDictionary *data, NSString *error))completion
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *newCityName = [cityName stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *requestString = [NSString stringWithFormat:@"data/2.5/weather?q=%@&APPID=%@",newCityName,AppID];
    self.lastTask = [self GET:requestString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         self.lastTask = nil;
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         if (responseObject)
         {
             if (completion)
             {
                 NSDictionary *data = responseObject;
                 completion(data, nil);
             }
         }
         else
         {
             if (completion) {
                 completion(nil,nil);
             }
         }
     }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          self.lastTask = nil;
          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
          if (completion)
          {
              completion(nil,error.localizedDescription);
          }
      }];
}

@end
