//
//  WeatherCityTableViewCell.h
//  WeatherApp
//
//  Created by Алексей on 21.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@interface WeatherCityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityCodeLabel;

- (void)setCellWithCity:(City *) city;

@end
