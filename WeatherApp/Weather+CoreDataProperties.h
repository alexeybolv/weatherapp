//
//  Weather+CoreDataProperties.h
//  WeatherApp
//
//  Created by Алексей on 23.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Weather.h"

NS_ASSUME_NONNULL_BEGIN

@interface Weather (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *weatherCityName;
@property (nullable, nonatomic, retain) NSString *weatherDescription;
@property (nullable, nonatomic, retain) NSString *weatherMain;
@property (nullable, nonatomic, retain) NSNumber *weatherTemp;
@property (nullable, nonatomic, retain) NSDate *weatherLastUpdate;

@end

NS_ASSUME_NONNULL_END
