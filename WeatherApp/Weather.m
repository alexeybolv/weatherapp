//
//  Weather.m
//  WeatherApp
//
//  Created by Алексей on 20.07.16.
//  Copyright © 2016 Alexey. All rights reserved.
//

#import "Weather.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation Weather

+ (Weather *)createWeatherWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context
{
    Weather *weather = [Weather MR_createEntityInContext:context];
    
    weather.weatherCityName = [dict objectForKey:@"name"];
    weather.weatherTemp = [NSNumber numberWithDouble:([[[dict objectForKey:@"main"] objectForKey:@"temp"] doubleValue] - 273.15)];
    weather.weatherMain = [[[dict objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"main"];
    weather.weatherDescription = [[[dict objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"];
    weather.weatherLastUpdate = [NSDate date];
    
    return weather;
}

- (void)changeWeatherWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context
{
    self.weatherTemp = [NSNumber numberWithDouble:([[[dict objectForKey:@"main"] objectForKey:@"temp"] doubleValue] - 273.15)];
    self.weatherMain = [[[dict objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"main"];
    self.weatherDescription = [[[dict objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"];
    self.weatherLastUpdate = [NSDate date];
}

@end
